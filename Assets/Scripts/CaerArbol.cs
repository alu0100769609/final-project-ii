﻿using UnityEngine;
using System.Collections;

public class CaerArbol : MonoBehaviour {
    private int VelocidadDeCaida = 50;
    private int TiempoAntesDeCaida = 0;
    // Use this for initialization
    void Start () {
    }

    // Update is called once per frame
    void Update () {
        TiempoAntesDeCaida++;
        if (TiempoAntesDeCaida >= 0 && TiempoAntesDeCaida <= 50)
            transform.Rotate(0, 0, - VelocidadDeCaida * Time.deltaTime);
    }
}
