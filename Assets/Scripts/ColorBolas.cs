﻿using UnityEngine;
using System.Collections;

public class ColorBolas : MonoBehaviour {
    public string ColorDeLaBola;
	// Use this for initialization
	void Start () {
        foreach (Renderer child in GetComponentsInChildren<Renderer>())
        {
            if (ColorDeLaBola == "white" || ColorDeLaBola == "White" || ColorDeLaBola == "Blanco" || ColorDeLaBola == "blanco")
                child.material.color = Color.white;
            else if (ColorDeLaBola == "blue" || ColorDeLaBola == "Blue" || ColorDeLaBola == "Azul" || ColorDeLaBola == "azul")
                child.material.color = Color.blue;
            else if (ColorDeLaBola == "red" || ColorDeLaBola == "Red" || ColorDeLaBola == "Rojo" || ColorDeLaBola == "rojo")
                child.material.color = Color.red;
            else if (ColorDeLaBola == "green" || ColorDeLaBola == "Green" || ColorDeLaBola == "Verde" || ColorDeLaBola == "verde")
                child.material.color = Color.green;
            else if (ColorDeLaBola == "yellow" || ColorDeLaBola == "Yellow" || ColorDeLaBola == "Amarillo" || ColorDeLaBola == "amarillo")
                child.material.color = Color.yellow;
            else
                child.material.color = Color.black;
        }
    }

    // Update is called once per frame
    void Update () {
	
	}
}
