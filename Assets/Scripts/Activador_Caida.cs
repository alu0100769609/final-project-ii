﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class Activador_Caida : MonoBehaviour {
    public List<GameObject> objetos; //lista de arboles
    public List<bool> cualesCaen;
    // Use this for initialization
    void Start () {
        //Recorrer objetos para ponerlos a false
        for (int i = 0; i < objetos.Count; i++)
            objetos[i].GetComponent<CaerArbol>().enabled = false;
    }

    void OnTriggerEnter()
    {
        //Recorrer objetos y dejamos caer los que estan a true
        for (int i = 0; i < objetos.Count; i++)
        {
            if(cualesCaen[i] == true)
                objetos[i].GetComponent<CaerArbol>().enabled = true;
        }
    }
	// Update is called once per frame
	void Update () {

	}
}
