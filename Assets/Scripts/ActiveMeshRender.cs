﻿using UnityEngine;
using System.Collections;

public class ActiveMeshRender : MonoBehaviour {
    public bool ActivarRender = true;
	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
        foreach (Transform child in gameObject.transform)
            child.GetComponent<MeshRenderer>().enabled = ActivarRender;
    }
}
