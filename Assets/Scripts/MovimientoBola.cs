﻿using UnityEngine;
using System.Collections;

public class MovimientoBola : MonoBehaviour {

    public int velocidad = 10;
    private bool movimientoDerecha = true;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

        if (movimientoDerecha == true)
        {
            transform.Translate(velocidad * Time.deltaTime, 0, 0);
            if (transform.position.x >= 8)
                movimientoDerecha = false;
        }
        else if (movimientoDerecha == false)
        {
            transform.Translate(-(velocidad * Time.deltaTime), 0, 0);
            if (transform.position.x <= -8)
                movimientoDerecha = true;
        }
    }
}
