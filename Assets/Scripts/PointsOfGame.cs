﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PointsOfGame : MonoBehaviour {

    private float puntos;
    public Text score;

	// Use this for initialization
	void Start () {
        puntos = 0;
	}
	
	// Update is called once per frame
	void Update () {
        puntos = puntos + (100 * Time.deltaTime);
        score.text = "Puntos: " + puntos.ToString();
	}

    void OnCollisionEnter()
    {
        if (puntos <= 100)
            puntos = 0;
        else
            puntos = puntos - 100;
    }
}
