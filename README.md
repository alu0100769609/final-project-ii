#Instrucciones de uso
Para ejecutar el juego sin errores se deben incluir, en este orden, las escenas **"Main"** y **"Escena1"** en el apartado **"Build settings..."**.

###Desde Unity
Si se ejecuta el juego desde Unity, para mover se utiliza el rat�n y la tecla **"Alt"** pulsada

###Desde M�vil
Mover el m�vil en el eje horizontal para cambiar la direcci�n. El objetivo es llegar a la meta esquivando �rboles y bolas, al chocar con un obst�culo se resta puntuaci�n.

##Hitos logrados:
- Personaje con movimiento constante
- Cambio de direcci�n del movimiento con la aplicaci�n Cardboard
- Objetos m�viles en la escena que funcionan de obst�culos
- Creaci�n de objetos activadores de eventos
- M�todo de puntuaci�n con penalizaciones por choque
- Modificaci�n del entorno infinito para que parezca realista
- Creaci�n de diferentes escenas
- A�adido el bot�n **"Exit"** necesario para poder salir de las aplicaciones de Unity.


##Enlaces

[Repositorio en bitbucket][1]

[1]: https://bitbucket.org/alu0100769609/final-project-ii

##Integrantes
- Adexe Sabina
- V�ctor Hern�ndez
- Eduardo Brito